package org.wlld;

import org.wlld.position.Position;
import org.wlld.position.PositionOperation;

import java.util.Random;

/**
 * Hello world!
 */
public class TestCase {
    public static void main(String[] args) throws Exception {
        test2();
    }

    public static void test2() throws Exception {
        Random random = new Random();
        PositionOperation operation = new PositionOperation();
        for (int i = 0; i < 18000; i++) {
            Position position = new Position();
            position.setX(random.nextInt(1000) - 500);
            position.setY(random.nextInt(1000) - 500);
            position.setUid(i);
            operation.insertXY(position);
        }
        operation.start();
        int testNub = 100;
        int rightNub = 0;
        for (int i = 0; i < testNub; i++) {
            int x = random.nextInt(998) - 500;
            int y = random.nextInt(998) - 500;
            int[] region = operation.getRegion(x, y);
            double value = operation.getDist(x, y);
            if (value >= region[0] && value <= region[1]) {
                rightNub++;
            }
        }
        double rightPoint = (double) rightNub / testNub;
        System.out.println("精准率:" + rightPoint);
    }
}
