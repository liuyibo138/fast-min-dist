package org.wlld.entity;

/**
 * @param
 * @DATA
 * @Author LiDaPeng
 * @Description 边缘实体类
 */
public  class NormPosition {
    private double normX;
    private double normY;

    public double getNormX() {
        return normX;
    }

    public void setNormX(double normX) {
        this.normX = normX;
    }

    public double getNormY() {
        return normY;
    }

    public void setNormY(double normY) {
        this.normY = normY;
    }
}
